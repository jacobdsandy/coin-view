# Coin View

Coin View allows you to search and view the details of thousands of Cryptocurrencies. It also allows you to add your Crypto holdings to a wallet and view timescale-based information.

## Running the Application

1. To run the application, you must first have Node and NPM installed on your machine.
2. You will also need to have Angular CLI installed.
   - If you do not have Angular CLI installed, run `npm install -g @angular/cli`.
3. Clone this repository onto your machine and navigate to the project directory.
   - `git clone https://gitlab.com/jacobdsandy/coin-view.git`
4. Install dependencies using `npm install --legacy-peer-deps`
5. Run `ng serve` to start the server.
6. Navigate to `http://localhost:4200/` to view the application.

## Using the Application

1. The home page will display a list of Cryptocurrencies.
   - The table supports searching on all columns as well as sorting each column by Ascending or Descending order
2. Clicking on a Cryptocurrency will take you to it's details page.
   - The details page will display some extra information about this Cryptocurrency
   - The details page also shows timescale-based information about the price of this Cryptocurrency over several intervals
   - Use the interval selector to update the graph
   - On the right side, you will see your Wallet. Here, you can view your current holdings, delete a holding, and add the displayed Cryptocurrency to your Wallet
3. The Wallet Graph is a menu item at the top of the nav bar. You can also get to your Wallet Graph by clicking "Go to Wallet Graph" from the Wallet details.
   - The Wallet Graph displays timescale-based information about your Wallet's performance
   - You can use the interval selector to update the graph and see different time frames
   - From here, you can also see your Wallet Details, where you can delete holdings

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
