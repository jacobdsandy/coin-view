import { DatePipe } from '@angular/common';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CoinWalletGraphComponent } from './coin-wallet-graph.component';

describe('CoinWalletGraphComponent', () => {
  let component: CoinWalletGraphComponent;
  let fixture: ComponentFixture<CoinWalletGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoinWalletGraphComponent],
      imports: [RouterTestingModule],
      providers: [HttpClient, HttpHandler, DatePipe],
    }).compileComponents();

    fixture = TestBed.createComponent(CoinWalletGraphComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
