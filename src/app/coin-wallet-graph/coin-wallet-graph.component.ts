import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartConfiguration } from 'chart.js';
import { forkJoin, Observable, take } from 'rxjs';
import { CoinHistory } from '../core/models/coin-history.model';
import { HoldingDetail } from '../core/models/holding-detail.model';
import { CoinService } from '../core/services/coin.service';

@Component({
  selector: 'cv-coin-wallet-graph',
  templateUrl: './coin-wallet-graph.component.html',
  styleUrls: ['./coin-wallet-graph.component.scss'],
})
export class CoinWalletGraphComponent implements OnInit {
  interval: string = 'h2';

  currentHoldings: Array<HoldingDetail>;

  lineChartData: ChartConfiguration['data'] | null;

  lineChartOptions: ChartConfiguration['options'];

  constructor(
    private coinService: CoinService,
    private datePipe: DatePipe,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.currentHoldings =
      JSON.parse(localStorage.getItem(CoinService.HoldingsKey)) ||
      new Array<HoldingDetail>();

    if (this.currentHoldings.length > 0) {
      this.getWalletHistory();
    }
  }

  onIntervalChanged(interval: string) {
    this.interval = interval;
    this.getWalletHistory();
  }

  onItemDeleted() {
    this.currentHoldings = JSON.parse(
      localStorage.getItem(CoinService.HoldingsKey)
    );
    this.getWalletHistory();
  }

  private getWalletHistory() {
    const start = this.getStartOfInterval(this.interval);
    const end = Date.now();
    const ids = this.currentHoldings.map((c) => c.id);
    const historyObservables = new Array<Observable<CoinHistory>>();
    ids.forEach((id) => {
      historyObservables.push(
        this.coinService.getCoinHistory(id, this.interval, start, end)
      );
    });

    if (historyObservables.length === 0) {
      this.lineChartData = null;
    } else {
      forkJoin(historyObservables)
        .pipe(take(1))
        .subscribe({
          next: (coinHistories: CoinHistory[]) => {
            this.buildWalletChart(coinHistories);
          },
          error: (err: any) => {
            this.router.navigate(['/']);
          },
        });
    }
  }

  private buildWalletChart(coinHistories: CoinHistory[]) {
    const data = this.getDataArray(coinHistories);
    const labels = this.getLabelsArray(coinHistories);
    this.lineChartData = {
      datasets: [
        {
          data: data,
          borderColor: '#673ab7',
          pointBackgroundColor: '#673ab7',
        },
      ],
      labels: labels,
    };

    this.lineChartOptions = {
      plugins: {
        legend: { display: false },
      },
    };
  }

  private getDataArray(coinHistories: CoinHistory[]) {
    const data = new Array(coinHistories[0].data.length).fill(0);
    this.currentHoldings.forEach((holding, i) => {
      const amount = holding.amount;
      const history = coinHistories[i];
      history.data.forEach((h, j) => {
        data[j] = data[j] + +h.priceUsd * amount;
      });
    });
    return data;
  }

  private getLabelsArray(coinHistories: CoinHistory[]) {
    let labels: Array<string>;
    if (
      this.interval === 'h6' ||
      this.interval === 'h12' ||
      this.interval === 'd1'
    ) {
      labels = coinHistories[0].data.map((history) =>
        new Date(history.date).toLocaleDateString()
      );
    } else if (this.interval === 'h2') {
      labels = coinHistories[0].data.map((history) =>
        this.datePipe.transform(new Date(history.date), 'MM/dd hh:mm a')
      );
    } else {
      labels = coinHistories[0].data.map((history) =>
        this.datePipe.transform(new Date(history.date), 'hh:mm a')
      );
    }
    return labels;
  }

  private getStartOfInterval(interval: string): number {
    const todaysDate = new Date();
    if (interval === 'm5') {
      todaysDate.setDate(todaysDate.getDate() - 1);
      return todaysDate.getTime();
    } else if (interval === 'h2') {
      todaysDate.setDate(todaysDate.getDate() - 7);
      return todaysDate.getTime();
    } else if (interval === 'h6') {
      todaysDate.setDate(todaysDate.getDate() - 30);
      return todaysDate.getTime();
    } else if (interval === 'h12') {
      todaysDate.setDate(todaysDate.getDate() - 90);
      return todaysDate.getTime();
    } else {
      todaysDate.setDate(todaysDate.getDate() - 365);
      return todaysDate.getTime();
    }
  }
}
