import { Component, OnInit, ViewChild } from '@angular/core';
import { forkJoin, map, Subject, takeUntil } from 'rxjs';
import { Coin } from '../core/models/coin.model';
import { CoinService } from '../core/services/coin.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';

@Component({
  selector: 'cv-coin-list',
  templateUrl: './coin-list.component.html',
  styleUrls: ['./coin-list.component.scss'],
})
export class CoinListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  dataSource = new MatTableDataSource<Coin>();

  coins: Array<Coin>;

  displayedColumns: string[] = [
    'rank',
    'symbol',
    'name',
    'priceUsd',
    'changePercent24Hr',
    'marketCapUsd',
    'volumeUsd24Hr',
  ];

  private unsubscribe = new Subject<void>();

  constructor(private router: Router, private coinService: CoinService) {}

  ngOnInit(): void {
    const firstSetOfCoins = this.coinService.getCoins(0, 2000);
    const lastSetOfCoin = this.coinService.getCoins(2000, 500);
    forkJoin([firstSetOfCoins, lastSetOfCoin])
      .pipe(
        map(([firstResponse, lastResponse]) => [
          firstResponse.data,
          lastResponse.data,
        ]),
        takeUntil(this.unsubscribe)
      )
      .subscribe({
        next: ([firstCoinList, lastCoinList]: [Array<Coin>, Array<Coin>]) => {
          this.coins = firstCoinList.concat(lastCoinList);
          this.dataSource.data = this.coins;
        },
        error: (err: any) => {
          this.router.navigate(['/']);
        },
      });
  }

  ngAfterViewInit() {
    this.dataSource.data = this.coins;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      if (
        property === 'rank' ||
        property === 'priceUsd' ||
        property === 'changePercent24Hr' ||
        property === 'marketCapUsd' ||
        property === 'volumeUsd24Hr'
      ) {
        return +item[property];
      }
      return item[property];
    };
  }

  getRoundedPrice(price: number) {
    if (price < 0.0001) return price;
    else if (price < 1) return +price.toFixed(4);
    else return +price.toFixed(2);
  }

  onSearchTextChanged(searchText: string) {
    this.dataSource.filter = searchText;
    this.dataSource.filterPredicate = (item, filter) => {
      let foundMatch = false;
      this.displayedColumns.forEach((column) => {
        if (
          item[column] !== null &&
          item[column]
            .toLocaleLowerCase()
            .indexOf(filter.toLocaleLowerCase()) != -1
        ) {
          foundMatch = true;
        }
      });
      return foundMatch;
    };
  }

  openRowDetails(coin: Coin) {
    this.router.navigate(['details', coin.id]);
  }
}
