import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ChartConfiguration } from 'chart.js';
import {
  CoinHistory,
  CoinHistoryDetail,
} from '../core/models/coin-history.model';

@Component({
  selector: 'cv-coin-detail-graph',
  templateUrl: './coin-detail-graph.component.html',
  styleUrls: ['./coin-detail-graph.component.scss'],
})
export class CoinDetailGraphComponent {
  @Input() name: string;

  @Input() set coinHistory(coinHistory: CoinHistory) {
    this.buildHistoryChart(coinHistory);
  }

  @Input() interval: string;

  @Output() intervalChanged = new EventEmitter<string>();

  lineChartData: ChartConfiguration['data'];

  lineChartOptions: ChartConfiguration['options'];

  constructor(private datePipe: DatePipe) {}

  onIntervalChanged(interval: string): void {
    this.intervalChanged.emit(interval);
  }

  private buildHistoryChart(coinHistory: CoinHistory) {
    const data = this.getDataArray(coinHistory.data);
    const labels = this.getLabelsArray(coinHistory.data);
    this.lineChartData = {
      datasets: [
        {
          data: data,
          borderColor: '#673ab7',
          pointBackgroundColor: '#673ab7',
        },
      ],
      labels: labels,
    };

    this.lineChartOptions = {
      plugins: {
        legend: { display: false },
      },
    };
  }

  private getDataArray(coinHistoryDetails: Array<CoinHistoryDetail>) {
    const data = coinHistoryDetails.map((history) => +history.priceUsd);
    return data;
  }

  private getLabelsArray(coinHistoryDetails: Array<CoinHistoryDetail>) {
    let labels: Array<string>;
    if (
      this.interval === 'h6' ||
      this.interval === 'h12' ||
      this.interval === 'd1'
    ) {
      labels = coinHistoryDetails.map((history) =>
        new Date(history.date).toLocaleDateString()
      );
    } else if (this.interval === 'h2') {
      labels = coinHistoryDetails.map((history) =>
        this.datePipe.transform(new Date(history.date), 'MM/dd hh:mm a')
      );
    } else {
      labels = coinHistoryDetails.map((history) =>
        this.datePipe.transform(new Date(history.date), 'hh:mm a')
      );
    }
    return labels;
  }
}
