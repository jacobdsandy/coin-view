import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinDetailGraphComponent } from './coin-detail-graph.component';

describe('CoinDetailGraphComponent', () => {
  let component: CoinDetailGraphComponent;
  let fixture: ComponentFixture<CoinDetailGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoinDetailGraphComponent],
      providers: [DatePipe],
    }).compileComponents();

    fixture = TestBed.createComponent(CoinDetailGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
