import { EventEmitter } from '@angular/core';
import { Component, Output } from '@angular/core';

@Component({
  selector: 'cv-coin-list-search-bar',
  templateUrl: './coin-list-search-bar.component.html',
  styleUrls: ['./coin-list-search-bar.component.scss'],
})
export class CoinListSearchBarComponent {
  @Output() searchTextChanged = new EventEmitter<string>();

  search: string;

  constructor() {}

  onChange(search: string) {
    this.searchTextChanged.emit(search);
  }
}
