import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CoinListSearchBarComponent } from './coin-list-search-bar.component';

describe('CoinListSearchBarComponent', () => {
  let component: CoinListSearchBarComponent;
  let fixture: ComponentFixture<CoinListSearchBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoinListSearchBarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CoinListSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
