import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CoinHistory } from '../models/coin-history.model';
import { Coin } from '../models/coin.model';
import { Response } from '../models/response.model';

@Injectable({
  providedIn: 'root',
})
export class CoinService {
  static readonly CoinResource = 'https://api.coincap.io/v2';

  static readonly HoldingsKey = 'holdings';

  constructor(private http: HttpClient) {}

  getCoins(
    offset: number = 0,
    limit: number = 2000,
    ids?: Array<string>
  ): Observable<Response<Coin>> {
    let params = new HttpParams().set('limit', limit).set('offset', offset);

    if (ids) {
      params = new HttpParams()
        .set('limit', limit)
        .set('offset', offset)
        .set('ids', ids.toLocaleString());
    }

    return this.http.get<Response<Coin>>(`${CoinService.CoinResource}/assets`, {
      params: params,
    });
  }

  getCoin(id: string): Observable<Response<Coin>> {
    let params = new HttpParams().set('limit', 2000);

    return this.http.get<Response<Coin>>(
      `${CoinService.CoinResource}/assets/${id}`,
      {
        params: params,
      }
    );
  }

  getCoinHistory(id: string, interval: string, start?: number, end?: number) {
    let params = new HttpParams().set('interval', interval);

    if (start && end) {
      params = new HttpParams()
        .set('interval', interval)
        .set('start', start)
        .set('end', end);
    }

    return this.http.get<CoinHistory>(
      `${CoinService.CoinResource}/assets/${id}/history`,
      {
        params: params,
      }
    );
  }
}
