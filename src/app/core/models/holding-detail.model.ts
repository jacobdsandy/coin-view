export interface HoldingDetail {
  id: string;
  symbol: string;
  amount: number;
  currentValue: number;
}
