export interface CoinHistory {
  data: Array<CoinHistoryDetail>;
}

export interface CoinHistoryDetail {
  date: string;
  priceUsd: string;
  time: number;
}
