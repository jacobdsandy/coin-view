export interface Response<T> {
  data: Array<T> | T;
}
