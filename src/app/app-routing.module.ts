import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoinDetailComponent } from './coin-detail/coin-detail.component';
import { CoinListComponent } from './coin-list/coin-list.component';
import { CoinWalletGraphComponent } from './coin-wallet-graph/coin-wallet-graph.component';

const routes: Routes = [
  {
    path: '',
    component: CoinListComponent,
  },
  {
    path: 'wallet',
    component: CoinWalletGraphComponent,
  },
  {
    path: 'details/:id',
    component: CoinDetailComponent,
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
