import { Overlay } from '@angular/cdk/overlay';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';

import { CoinWalletComponent } from './coin-wallet.component';

describe('CoinWalletComponent', () => {
  let component: CoinWalletComponent;
  let fixture: ComponentFixture<CoinWalletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CoinWalletComponent],
      providers: [MatSnackBar, Overlay, HttpClient, HttpHandler],
    }).compileComponents();

    fixture = TestBed.createComponent(CoinWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
