import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { map, take } from 'rxjs';
import { Coin } from '../core/models/coin.model';
import { HoldingDetail } from '../core/models/holding-detail.model';
import { CoinService } from '../core/services/coin.service';

@Component({
  selector: 'cv-coin-wallet',
  templateUrl: './coin-wallet.component.html',
  styleUrls: ['./coin-wallet.component.scss'],
})
export class CoinWalletComponent implements OnInit {
  @Input() symbol: string;

  @Input() id: string;

  @Output() itemDeleted = new EventEmitter<string>();

  amount: number | null;

  currentHoldings: Array<HoldingDetail>;

  totalValue: number = 0;

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private coinService: CoinService
  ) {}

  ngOnInit(): void {
    this.currentHoldings = JSON.parse(
      localStorage.getItem(CoinService.HoldingsKey)
    );
    if (!this.currentHoldings) {
      this.currentHoldings = new Array<HoldingDetail>();
    } else {
      this.getTotalValue();
    }
  }

  onAdd() {
    if (!this.amount) {
      return;
    }
    const foundItem = this.currentHoldings.find(
      (h) => h.symbol === this.symbol
    );
    if (foundItem) {
      foundItem.amount += this.amount;
    } else {
      this.currentHoldings.push({
        id: this.id,
        symbol: this.symbol,
        amount: this.amount,
        currentValue: 0,
      });
    }

    this.currentHoldings.forEach((holding, index) => {
      if (holding.amount <= 0) {
        this.currentHoldings.splice(index, 1);
      }
    });
    localStorage.setItem(
      CoinService.HoldingsKey,
      JSON.stringify(this.currentHoldings)
    );
    this.openSnackBar(`Successfully added ${this.amount} ${this.symbol}`);
    this.amount = null;
    this.getTotalValue();
  }

  onDelete(symbol: string) {
    this.currentHoldings.forEach((holding, index) => {
      if (holding.symbol === symbol) {
        this.currentHoldings.splice(index, 1);
      }
    });
    localStorage.setItem(
      CoinService.HoldingsKey,
      JSON.stringify(this.currentHoldings)
    );
    this.openSnackBar(`Successfully deleted ${symbol}`);
    this.getTotalValue();
    this.itemDeleted.emit(symbol);
  }

  private getTotalValue() {
    if (this.currentHoldings.length === 0) {
      this.totalValue = 0;
      return;
    }
    const ids = this.currentHoldings.map((h) => h.id);
    this.coinService
      .getCoins(0, 2000, ids)
      .pipe(
        map((response) => response.data),
        take(1)
      )
      .subscribe({
        next: (coins: Array<Coin>) => {
          let value = 0;
          coins.forEach((c) => {
            const holding = this.currentHoldings.find(
              (holding) => holding.id === c.id
            );
            const holdingValue = +c.priceUsd * holding.amount;
            holding.currentValue = holdingValue;
            value += holdingValue;
          });
          this.totalValue = value;
        },
        error: (err: any) => {
          this.router.navigate(['/']);
        },
      });
  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
    });
  }
}
