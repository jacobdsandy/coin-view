import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { forkJoin, Subject, take, takeUntil } from 'rxjs';
import { CoinHistory } from '../core/models/coin-history.model';
import { Coin } from '../core/models/coin.model';
import { CoinService } from '../core/services/coin.service';

@Component({
  selector: 'cv-coin-detail',
  templateUrl: './coin-detail.component.html',
  styleUrls: ['./coin-detail.component.scss'],
})
export class CoinDetailComponent implements OnInit, OnDestroy {
  coin: Coin;

  coinHistory: CoinHistory;

  interval: string = 'h2';

  private unsubscribe = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private coinService: CoinService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((params: Params) => {
        if (params['id']) {
          this.getCoinDetails(params['id']);
        } else {
          this.router.navigate(['/']);
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
  }

  getRoundedPrice(price: number) {
    if (price < 0.0001) return price;
    else if (price < 1) return +price.toFixed(4);
    else return +price.toFixed(2);
  }

  onIntervalChanged(interval: string) {
    this.interval = interval;
    const start = this.getStartOfInterval(interval);
    const end = Date.now();
    this.coinService
      .getCoinHistory(this.coin.id, interval, start, end)
      .pipe(take(1))
      .subscribe({
        next: (coinHistory: CoinHistory) => {
          this.coinHistory = coinHistory;
        },
        error: (err: any) => {
          this.router.navigate(['/']);
        },
      });
  }

  private getStartOfInterval(interval: string): number {
    const todaysDate = new Date();
    if (interval === 'm5') {
      todaysDate.setDate(todaysDate.getDate() - 1);
      return todaysDate.getTime();
    } else if (interval === 'h2') {
      todaysDate.setDate(todaysDate.getDate() - 7);
      return todaysDate.getTime();
    } else if (interval === 'h6') {
      todaysDate.setDate(todaysDate.getDate() - 30);
      return todaysDate.getTime();
    } else if (interval === 'h12') {
      todaysDate.setDate(todaysDate.getDate() - 90);
      return todaysDate.getTime();
    } else {
      todaysDate.setDate(todaysDate.getDate() - 365);
      return todaysDate.getTime();
    }
  }

  private getCoinDetails(id: string) {
    const start = this.getStartOfInterval(this.interval);
    const end = Date.now();
    const coinDetails = this.coinService.getCoin(id);
    const coinHistory = this.coinService.getCoinHistory(
      id,
      this.interval,
      start,
      end
    );

    forkJoin([coinDetails, coinHistory])
      .pipe(take(1))
      .subscribe({
        next: ([coin, coinHistory]) => {
          this.coin = coin.data as Coin;
          this.coinHistory = coinHistory;
        },
        error: (err: any) => {
          this.router.navigate(['/']);
        },
      });
  }
}
