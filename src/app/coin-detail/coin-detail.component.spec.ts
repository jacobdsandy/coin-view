import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { CoinDetailComponent } from './coin-detail.component';

describe('CoinDetailsComponent', () => {
  let component: CoinDetailComponent;
  let fixture: ComponentFixture<CoinDetailComponent>;

  // const activatedRouteSpy = jasmine.createSpyObj('activatedRoute',['params']);

  const fakeActivatedRoute = {
    params: of({
      id: 'bitcoin',
    }),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoinDetailComponent],
      imports: [RouterTestingModule],
      providers: [
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
        HttpClient,
        HttpHandler,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CoinDetailComponent);
    component = fixture.componentInstance;

    // activatedRouteSpy.params.and.returnValue(of({id: 'bitcoin'}))

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
